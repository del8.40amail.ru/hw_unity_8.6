using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingObject : MonoBehaviour
{
    public enum MovementType
    {
        Moveing,
        Lerping
    }

    private Animator animator;
    public MovementType Type = MovementType.Moveing;
    public MovingThroughPoints MyPath;
    public float speed = 0;
    private float maxDistance = .1f;
    public GameObject[] people;
    private int peopleIndOne = 0;
    private int peopleIndTwo = 1;

    private IEnumerator<Transform> pointInPath;
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("BoolRunTrigger", false);

        for (int i = 0; i < people.Length; i++)
        {
            people[i].transform.position = MyPath.PathElements[i].position;
        }
    }

    void Update()
    {
        if (peopleIndOne > people.Length-1)
        {
            peopleIndOne = 0;
        }
        if (peopleIndTwo>people.Length-1)
        {
            peopleIndTwo = 0;
        }
        if (Type ==MovementType.Moveing)
        {
            people[peopleIndOne].transform.position = Vector3.MoveTowards(people[peopleIndOne].transform.position, people[peopleIndTwo].transform.position, Time.deltaTime * speed);
            people[peopleIndOne].transform.LookAt(people[peopleIndTwo].transform.position);
        }
        else if (Type == MovementType.Lerping)
        {
            people[peopleIndOne].transform.position = Vector3.Lerp(people[peopleIndOne].transform.position, people[peopleIndTwo].transform.position, Time.deltaTime * speed);
            people[peopleIndOne].transform.LookAt(people[peopleIndTwo].transform.position);
        }
        var distanceSqure = (people[peopleIndOne].transform.position - people[peopleIndTwo].transform.position).sqrMagnitude;
        if (distanceSqure < maxDistance * maxDistance)
        {
            peopleIndOne++;
            peopleIndTwo++;
        }
    }

    public void ClickStart()
    {
        animator.SetBool("BoolRunTrigger", true);
        speed = 1;
    }
    public void ClickStop()
    {
        speed = 0;
        animator.SetBool("BoolRunTrigger", false);
    }
}
