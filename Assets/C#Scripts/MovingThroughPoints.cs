using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingThroughPoints : MonoBehaviour
{
    public enum PathTypes // ���� ����: �������� ��� �����������
    {
        linear,
        loop
    }
    public PathTypes pathType; //���������� ��� ����
    public int movementDirection = 1; //����������� ��������: ������ ��� �����
    public int movengTo = 0; // � ����� ����� ��������� 
    public Transform[] PathElements; // ������ ����� ��������

    public void OnDrawGizmos() // ����������� ����� ����� ������� ����
    {
        if(PathElements== null || PathElements.Length<2) // ��������� ���� �� ��� ������� ��� �������� ����
        {
            return;
        }
        for (var i = 1; i < PathElements.Length; i++) //��������� ��� ����� �������
        {
            Gizmos.DrawLine(PathElements[i - 1].position, PathElements[i].position); //������ ����� ����� �������
        }

        if(pathType == PathTypes.loop) // ���� ���� �������
        {
            Gizmos.DrawLine(PathElements[0].position, PathElements[PathElements.Length-1].position); //������ ���� ����� ��������� � 0 ������
        }
    }

    public IEnumerator<Transform> GetNextPathPoint() //��������� ��������� ��������� �����
    { 
        if(PathElements==null || PathElements.Length<1) // ���������, ���� �� ����� ��� ���� ����� ������ �����
        {
            yield break; // ������� �� �������� ���� ����� �������������
        }

        while (true)
        {
            yield return PathElements[movengTo]; //���������� ������� ��������� �����

            if (PathElements.Length ==1) //���� ����� ����, �� ���� �����
            {
                continue;
            }
            if( pathType ==PathTypes.linear) // ���� �� ����������� ����
            {
                if(movengTo<=0) // ���� ��������� �� �����������
                {
                    movementDirection = 1; //+1 � ��������
                }
                else if (movengTo >= PathElements.Length -1) // ���� �� ���������
                {
                    movementDirection = -1;  //-1 � ��������
                }
            } 
            movengTo = movengTo + movementDirection; //�������� �������� �� ������ �� ����� ������

            if(pathType ==PathTypes.loop) // ���� ���� �����������
            {
               if(movengTo >= PathElements.Length) // ���� ������ ����� �� ��������� ����� �������, �� ����� ����������� 0 �����
                {
                    movengTo = 0;
                }

               if(movengTo <0) //���� ������ ����� �� ������ ����� �������� � �������� �������, �� ���� ��������� � ��������� �����
                {
                    movengTo = PathElements.Length - 1;
                }
            }
        }
    }
}
