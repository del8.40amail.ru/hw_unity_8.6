using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingObjectsPoints : MonoBehaviour
{
    public enum MovementType
    {
        Moveing,
        Lerping
    }

    private Animator animator;
    public MovementType Type = MovementType.Moveing; //��� ��������
    public MovingThroughPoints MyPath; // ������������ ����
    public float speed = 0;
    private float maxDistance = .1f;
    public GameObject[] people;

    private IEnumerator<Transform> pointInPath; //�������� ����� 
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("BoolRunTrigger", false);
        if (MyPath == null) //���������� �� ����
        {
            Debug.Log("���� ��������� ����");
            return;
        }

        pointInPath = MyPath.GetNextPathPoint();
        pointInPath.MoveNext();

        if (pointInPath.Current == null)
        {
            Debug.Log("����� �����");
            return;
        }

        transform.position = pointInPath.Current.position;

        for (int i = 0; i < people.Length; i++) // ����������� ������� ������ �� �����
        {
            people[i].transform.position = MyPath.PathElements[i].position;
        }
    }

    void Update()
    {
        if (pointInPath == null || pointInPath.Current == null)
        {
            return;
        }

        if (Type == MovementType.Moveing)
        {
            transform.position = Vector3.MoveTowards(transform.position, pointInPath.Current.position, Time.deltaTime * speed);
            transform.LookAt(pointInPath.Current.position);

        }
        else if (Type == MovementType.Lerping)
        {
            transform.position = Vector3.Lerp(transform.position, pointInPath.Current.position, Time.deltaTime * speed);
            transform.LookAt(pointInPath.Current.position);
        }

        var distanceSqure = (transform.position - pointInPath.Current.position).sqrMagnitude;
        if (distanceSqure < maxDistance * maxDistance)
        {
            pointInPath.MoveNext();
        }
    }

    public void ClickStart()
    {
        animator.SetBool("BoolRunTrigger", true);
        speed = 1;
    }
    public void ClickStop()
    {
        speed = 0;
        animator.SetBool("BoolRunTrigger", false);
    }
}
